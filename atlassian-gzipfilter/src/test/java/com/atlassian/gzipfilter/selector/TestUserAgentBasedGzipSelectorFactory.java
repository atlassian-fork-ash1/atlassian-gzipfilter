package com.atlassian.gzipfilter.selector;

import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;

import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUserAgentBasedGzipSelectorFactory extends TestCase
{
    final FilterConfig filterConfig = mock(FilterConfig.class);
    final HttpServletRequest request = mock(HttpServletRequest.class);

    public void testNoopConfig()
    {
        final UserAgentBasedGzipSelectorFactory factory = new UserAgentBasedGzipSelectorFactory(filterConfig);

        when(request.getHeader(UserAgentBasedGzipSelectorFactory.USER_AGENT_HEADER)).thenReturn("MSIE 6.1b");
        final GzipCompatibilitySelector ie6Selector = factory.getSelector(filterConfig, request);
        assertFalse(ie6Selector.shouldGzip());

        when(request.getHeader(UserAgentBasedGzipSelectorFactory.USER_AGENT_HEADER)).thenReturn("Chrome");
        final GzipCompatibilitySelector chromeSelector = factory.getSelector(filterConfig, request);
        assertTrue(chromeSelector.shouldGzip());
        assertTrue(chromeSelector.shouldGzip("text/html"));
        assertFalse(chromeSelector.shouldGzip("gzip"));
    }

    public void testConfigOverride()
    {
        when(filterConfig.getInitParameter(UserAgentBasedGzipSelectorFactory.COMPRESSABLE_MIME_TYPES_PARAM_NAME))
                .thenReturn("text/xml,text/html");
        when(filterConfig.getInitParameter(UserAgentBasedGzipSelectorFactory.NO_COMPRESSION_USER_AGENTS_PARAM_NAME))
                .thenReturn("Firefox,Chrome");

        final UserAgentBasedGzipSelectorFactory factory = new UserAgentBasedGzipSelectorFactory(filterConfig);

        when(request.getHeader(UserAgentBasedGzipSelectorFactory.USER_AGENT_HEADER)).thenReturn("Firefox");
        final GzipCompatibilitySelector firefoxSelector = factory.getSelector(filterConfig, request);
        assertFalse(firefoxSelector.shouldGzip());

        when(request.getHeader(UserAgentBasedGzipSelectorFactory.USER_AGENT_HEADER)).thenReturn("Firefox");
        final GzipCompatibilitySelector chromeSelector = factory.getSelector(filterConfig, request);
        assertFalse(chromeSelector.shouldGzip());

        when(request.getHeader(UserAgentBasedGzipSelectorFactory.USER_AGENT_HEADER)).thenReturn("MSIE");
        final GzipCompatibilitySelector msieSelector = factory.getSelector(filterConfig, request);
        assertTrue(msieSelector.shouldGzip());
        assertTrue(msieSelector.shouldGzip("text/xml"));
        assertTrue(msieSelector.shouldGzip("text/html"));
        assertFalse(msieSelector.shouldGzip("text/plain"));
    }
}
