package com.atlassian.gzipfilter.selector;

import junit.framework.TestCase;

public class TestPatternMatcher extends TestCase
{
    PatternMatcher p = new PatternMatcher();

    public void testSimple()
    {
        assertTrue(p.matches("text/html", "text/html"));
        assertTrue(p.matches("text/plain", "text/plain"));

        assertFalse(p.matches("text/plain", "text/html"));
        assertFalse(p.matches("text/plain", ""));
        assertFalse(p.matches("text/plain", ",,,,"));
    }

    public void testMultipleInputs()
    {
        assertTrue(p.matches("text/html", "text/plain,text/html"));
        assertTrue(p.matches("text/plain", "text/plain,text/html"));
        assertTrue(p.matches("text/plain", "text/plain,text/html,application/x-javascript"));

        assertFalse(p.matches("application/msword", "text/plain,text/html,application/x-javascript"));
        assertFalse(p.matches("text/javascript", "text/plain,text/html,application/x-javascript"));
    }

    public void testRegularExpressions()
    {
        assertTrue(p.matches("text/html", "text/.*"));
        assertTrue(p.matches("text/html", "application/x-javascript,text/.*"));
        assertTrue(p.matches("text/plain", "application/x-javascript,text/.*"));
        assertTrue(p.matches("application/javascript", "application/.?.?javascript,text/.*"));
        assertTrue(p.matches("application/x-javascript", "application/.?.?javascript,text/.*"));

        assertFalse(p.matches("application/msword", "application/??javascript,text/.*"));
        assertFalse(p.matches("application/msword", "application/??javascript,text/.*"));
    }

    static final String TYPES_TO_GZIP = "text/.*,application/.?.?javascript,application/xml";

    public void testForRealWorldUsage()
    {
        // text/html,text/javascript,text/css,text/plain,application/x-javascript,application/javascript

        assertTrue(p.matches("text/html", TYPES_TO_GZIP));
        assertTrue(p.matches("text/plain", TYPES_TO_GZIP));
        assertTrue(p.matches("text/css", TYPES_TO_GZIP));
        assertTrue(p.matches("text/javascript", TYPES_TO_GZIP));
        assertTrue(p.matches("application/javascript", TYPES_TO_GZIP));
        assertTrue(p.matches("application/x-javascript", TYPES_TO_GZIP));
        assertTrue(p.matches("application/xml", TYPES_TO_GZIP));

        assertFalse(p.matches("application/msword", TYPES_TO_GZIP));
        assertFalse(p.matches("application/octet-stream", TYPES_TO_GZIP));
    }

    // Perfomance test to detemine whether caching works or not.
    // Commented out because it requires java 1.5 methods, but we want
    // this library to work with JDK 1.4.

//    public static void main(String[] args) throws InterruptedException
//    {
//        long start = System.currentTimeMillis();
//        java.util.concurrent.ExecutorService ex = java.util.concurrent.Executors.newFixedThreadPool(20);
//        for (int i = 0; i < 20; i++)
//        {
//            ex.execute(new PerfTestThread());
//        }
//        ex.shutdown();
//        ex.awaitTermination(1000, java.util.concurrent.TimeUnit.SECONDS);
//
//        System.out.println("Time taken: " + (System.currentTimeMillis() - start));
//    }
//
//    private static class PerfTestThread implements Runnable
//    {
//        public void run()
//        {
//            TestPatternMatcher tpm = new TestPatternMatcher();
//            System.out.println("Starting Thread");
//            // Performance testing
//            for (int i = 0; i < 10000; i++)
//            {
//                tpm.testForRealWorldUsage();
//            }
//            System.out.println("Finishing Thread");
//
//        }
//    }
}
