package com.atlassian.gzipfilter;

import java.io.File;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import com.atlassian.gzipfilter.integration.GzipFilterIntegration;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GzipFilterTest
{

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testChecksForLegacyFilterConfigInitParam() throws Exception
    {
        final FilterConfig filterConfig = mock(FilterConfig.class);
        final GzipFilterIntegration gzipFilterIntegration = mock(GzipFilterIntegration.class);

        final GzipFilter filter = new GzipFilter(gzipFilterIntegration);
        when(filterConfig.getInitParameter(GzipFilter.LEGACY_INIT_PARAM)).thenReturn("overwrite");

        expectedException.expect(IllegalArgumentException.class);
        filter.init(filterConfig);
    }

    @Test
    public void testChecksForLegacyConfigOverrideUnderWebInf() throws Exception
    {
        final ServletContext servletContext = mock(ServletContext.class);
        final FilterConfig filterConfig = mock(FilterConfig.class);
        final GzipFilterIntegration gzipFilterIntegration = mock(GzipFilterIntegration.class);
        when(filterConfig.getServletContext()).thenReturn(servletContext);

        final File file = File.createTempFile("url-rewrite", "xml");
        when(servletContext.getRealPath("/WEB-INF/" + GzipFilter.LEGACY_CONFIG_FILE)).thenReturn(file.getAbsolutePath());

        final GzipFilter filter = new GzipFilter(gzipFilterIntegration);

        expectedException.expect(IllegalArgumentException.class);
        filter.init(filterConfig);
    }

    @Test
    public void testNoLegacyOverrides() throws ServletException
    {
        final FilterConfig filterConfig = mock(FilterConfig.class);
        final GzipFilterIntegration gzipFilterIntegration = mock(GzipFilterIntegration.class);

        final GzipFilter filter = new GzipFilter(gzipFilterIntegration);
        filter.init(filterConfig);
    }
}
