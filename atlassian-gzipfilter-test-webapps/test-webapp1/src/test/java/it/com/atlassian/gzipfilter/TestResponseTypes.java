package it.com.atlassian.gzipfilter;

import junit.framework.TestCase;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestResponseTypes
{
    private HttpMethod method;

    @Before
    public void setUp() throws Exception
    {
        method = null;
    }

    @Test
    public void test304HasNoBody() throws IOException
    {
        int i = executeMethodAcceptingGzip("304.html");
        assertEquals("Should return 304 - not modified", 304, i);
        assertEmptyBody(method);
    }

    @Test
    public void testRedirect() throws IOException
    {
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(IntegrationTestUtils.URL + "redirect.html");
        method.addRequestHeader(IntegrationTestUtils.GZIP_ACCEPT_HEADER);
        method.setFollowRedirects(false);

        // test mime-type of image/png is not gzipped
        int i = client.executeMethod(method);
        assertEquals("Should return 302 - redirect", 302, i);

        // According to the HTTP spec, redirect should have a body:
        //   http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
        // "Unless the request method was HEAD, the entity of the response
        // SHOULD contain a short hypertext note with a hyperlink to the new URI(s)"
//        assertEmptyBody(method);
    }

    @Test
    public void testUrlRewriteClassesAreNotFound() throws IOException
    {
        int i = executeMethodAcceptingGzip("classes.html");
        assertEquals("Should return 200 if classes can not be found", 200, i);
    }



    private static void assertEmptyBody(HttpMethod method) throws IOException
    {
        if (method.getResponseBody() != null && method.getResponseBody().length > 0)
            fail("Content should be null, but was '" + new String(method.getResponseBody(), "UTF-8") + "'");
    }

    private int executeMethodAcceptingGzip(String s) throws IOException
    {
        HttpClient client = new HttpClient();
        method = new GetMethod(IntegrationTestUtils.URL + s);
        method.addRequestHeader(IntegrationTestUtils.GZIP_ACCEPT_HEADER);
        return client.executeMethod(method);
    }
}