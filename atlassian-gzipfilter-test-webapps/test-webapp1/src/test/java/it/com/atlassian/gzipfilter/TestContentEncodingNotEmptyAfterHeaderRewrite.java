package it.com.atlassian.gzipfilter;


import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpMethod;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collection;

@RunWith (Parameterized.class)
public class TestContentEncodingNotEmptyAfterHeaderRewrite
{
    @Parameterized.Parameters(name = "{index}: expected gzippable: {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new String[]{"application/octet-stream","application/javascript"}, true},
                { new String[]{"application/javascript","application/octet-stream"}, false},
                { new String[]{"text/html;charset=UTF-8","application/atom+xml"}, false},
        });
    }

    @Parameterized.Parameter(0)
    public String[] mimeTypesSequence;
    @Parameterized.Parameter(1)
    public boolean gzipped;

    public void testRewriteWith(String path, String outputMethod) throws IOException
    {
        StringBuilder sb = new StringBuilder(mimeTypesSequence[0]);
        for (int i=1; i<mimeTypesSequence.length; i++) {
            sb.append(",").append(mimeTypesSequence[i]);
        }
        HttpMethod method = IntegrationTestUtils.assertPathGzipped(
                path + "?outputMethod=" + outputMethod +
                        "&mimetypes=" + URLEncoder.encode(sb.toString(), "UTF-8"),
                null,
                gzipped
        );
        String expectedCtype = mimeTypesSequence[mimeTypesSequence.length - 1];
        Header contentTypeHeader = method.getResponseHeader("Content-Type");
        assertNotNull("Content-Type header is NULL, expected: "+expectedCtype, contentTypeHeader);
        assertThat(contentTypeHeader.getValue(), startsWith(expectedCtype));
    }

    @Test
    public void testRewritePrintWriter() throws IOException
    {
        testRewriteWith("rewriter.html", "PrintWriter");
    }

    @Test
    public void testRewriteOutputStream() throws IOException
    {
        testRewriteWith("rewriter.html", "OutputStream");
    }

    @Test
    public void testRewriteFilterPrintWriter() throws IOException
    {
        testRewriteWith("rewriter/noop.html", "PrintWriter");
    }

    @Test
    public void testRewriteFilterOutputStream() throws IOException
    {
        testRewriteWith("rewriter/noop.html", "OutputStream");
    }

    @Test
    public void testIncludeRewriterPrintWriter() throws IOException
    {
        testRewriteWith("include_rewriter.html", "PrintWriter");
    }

    @Test
    public void testIncludeRewriterOutputStream() throws IOException
    {
        testRewriteWith("include_rewriter.html", "OutputStream");
    }

    @Test
    @Ignore("This is how JIRA now uses the filter, and it's failing")
    public void testBrokenIncludeRewriterPrintWriter() throws IOException
    {
        testRewriteWith("broken/include_rewriter.html", "PrintWriter");
    }

    @Test
    @Ignore("This is how JIRA now uses the filter, and it's failing")
    public void testBrokenIncludeRewriterOutputStream() throws IOException
    {
        testRewriteWith("broken/include_rewriter.html", "OutputStream");
    }
}