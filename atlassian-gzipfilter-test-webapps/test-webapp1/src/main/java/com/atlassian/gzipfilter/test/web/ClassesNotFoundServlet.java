package com.atlassian.gzipfilter.test.web;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;

/**
 * A servlet that tests that the URLRewrite classes cannot be loaded.
 */
public class ClassesNotFoundServlet extends HttpServlet
{
    private static final String REWRITEFILTER_CLASSNAME = "org.tuckey.web.filters.urlrewrite.UrlRewriteFilter";

    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            this.getClass().getClassLoader().loadClass(REWRITEFILTER_CLASSNAME);
            resp.sendError(500, "Error.  Class '" + REWRITEFILTER_CLASSNAME + "' could be found.  Should have been hidden in build process");
        }
        catch (ClassNotFoundException e)
        {
            resp.setStatus(200);
            resp.getWriter().write("Success.  Classes could not be loaded.");
        }
    }
}