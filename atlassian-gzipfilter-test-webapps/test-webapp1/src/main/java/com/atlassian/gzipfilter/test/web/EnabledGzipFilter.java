package com.atlassian.gzipfilter.test.web;

import com.atlassian.gzipfilter.GzipFilter;
import com.atlassian.gzipfilter.integration.GzipFilterIntegration;

import javax.servlet.http.HttpServletRequest;

public class EnabledGzipFilter extends GzipFilter
{
    public EnabledGzipFilter()
    {
        super(new GzipFilterIntegration()
        {
            public boolean useGzip()
            {
                return true;
            }

            public String getResponseEncoding(HttpServletRequest request)
            {
                return "UTF-8";
            }
        });
    }
}
